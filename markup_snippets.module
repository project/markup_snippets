<?php

/*
 * Created by Greg Harvey on 15 Jul 2009
 *
 * http://www.drupaler.co.uk
 */

/**
 * Implementation of hook_form_alter().
 */
function markup_snippets_form_alter(&$form, $form_state, $form_id) {
  
  // make sure we're on a node form and user has permission to use snippets
  if ($form['#id'] == 'node-form' && user_access('use snippets')) {
    // enforce caching for ahah
    $form['#cache'] = TRUE;
    
    // load the content type settings
    $type = node_get_types('type', $form['#node']);
    
    // set up a new fieldset above the node body field
    $form['body_field']['markup_snippets'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mark-up Snippets'),
      '#description' => t('Load saved snippets or save the contents of the current !body_label field.', array('!body_label' => $type->body_label)),
      '#weight' => -100,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    
    // set variables for the select list
    $disabled = FALSE;
    $default_label = t('Select a snippet >>');
    $options = array($default_label);
    
    // try to load snippets
    if (markup_snippets_get_snippets()) {
      $options = markup_snippets_get_snippets($options);
      $description = t('Choose from the previously saved snippets. Pick one and it will automatically REPLACE the contents of the !body_label box.', array('!body_label' => $type->body_label));
    }
    // disable the field if no snippets are available
    else {
      $disabled = TRUE;
      $description = t('There are no snippets saved in the database yet.');
    }
    
    // render the actual form field
    $form['body_field']['markup_snippets']['markup_snippets_select'] = array(
      '#type' => 'select',
      '#title' => t('Select a snippet to use'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => 0,
      '#disabled' => $disabled,
      '#ahah' => array(
        'path' => 'markup_snippets/js',
        'wrapper' => 'edit-body-wrapper',
        // default so not needed: 'method' => 'replace',
        'event' => 'change',
      ),
    );
    
    // we need a field for naming our snippet
    $form['body_field']['markup_snippets']['markup_snippets_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter a name to save the current snippet'),
      '#size' => 30,
      '#description' => t('If you enter a name in this box then the contents of the !body_label field will be saved as a snippet when you click Save.', array('!body_label' => $type->body_label))
    );
    
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function markup_snippets_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'insert':
    case 'update':
      // if a name was entered we want to save the body field as a snippet
      if ($node->markup_snippets_name) {
        // load any existing snippet with that name
        // note, names do not technically need to be unique, but avoids UI confusion
        $query = db_query("SELECT mid FROM {markup_snippets} WHERE name = '%s'", $node->markup_snippets_name);
        $result = db_result($query);
        
        // if the name is in use, warn and do nothing
        if ($result) {
          drupal_set_message(t('Snippet was not saved because a snippet named %snippet already exists.', array('%snippet' => $node->markup_snippets_name)), 'error');
        }
        // otherwise save our snippet
        else {
          // make sure there's no mark-up in the name
          $snippet_name = check_plain($node->markup_snippets_name);
          // save the snippet
          db_query("INSERT INTO {markup_snippets} (name, snippet) VALUES ('%s', '%s')", $snippet_name, $node->body);
          drupal_set_message(t('New snippet named %snippet was saved.', array('%snippet' => $snippet_name)));
        }
      }
    break;
  }
}

/**
 * Implementation of hook_perm().
 */
function markup_snippets_perm() {
  return array('use snippets', 'administer snippets');
}

/**
 * Implementation of hook_menu().
 */
function markup_snippets_menu() {
  $items = array();
  
  $items['admin/content/markup_snippets'] = array(
    'title' => t('Mark-up Snippets'),
    'description' => t('Manage the mark-up snippets saved in the database.'),
    'page callback' => 'markup_snippets_admin',
    'access arguments' => array('administer snippets'),
  );
  
  $items['admin/content/markup_snippets/delete'] = array(
    'title' => t('Delete Mark-up Snippet'),
    'description' => t('Delete a mark-up snippet from the database.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('markup_snippets_admin_delete'),
    'access arguments' => array('administer snippets'),
  );
  
  $items['admin/content/markup_snippets/edit'] = array(
    'title' => t('Edit Mark-up Snippet'),
    'description' => t('Edit a mark-up snippet in the database.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('markup_snippets_admin_edit'),
    'access arguments' => array('administer snippets'),
  );
  
  $items['markup_snippets/js'] = array(
    'title' => 'Javascript Node Body Loader',
    'page callback' => 'markup_snippets_js',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  
  return $items;
}

/**
 * Delete form for snippets.
 */
function markup_snippets_admin_delete() {
  
  // set up a default form if we get bad data
  $bad_request_form = array();
  $bad_request_form['#prefix'] = l(t('Go to snippet list'), 'admin/content/markup_snippets');
  
  // check we have a snippet ID
  if (is_numeric(arg(4))) {
    $mid = arg(4);
  }
  // if not send bad request
  else {
    drupal_set_message(t('You must specify a valid snippet to delete.'), 'error');
    return $bad_request_form;
  }
  
  // attempt to load the snippet
  $snippet = markup_snippets_get_snippets(array(), FALSE, $mid);
  
  // if there's a snippet, load the delete form
  if ($snippet) {
    $form = array();
  
    $form['#prefix'] = t('Are you sure you want to delete the snippet %snippet?', array('%snippet' => $snippet[$mid]));
    
    $form['markup_snippets_mid'] = array(
      '#type' => 'value',
      '#value' => $mid,
    );
  
    $form['markup_snippets_delete'] = array(
      '#type' => 'submit',
      '#default_value' => t('Delete'),
    );
  
    $form['markup_snippets_delete_cancel'] = array(
      '#type' => 'markup',
      '#value' => l(t('Cancel'), 'admin/content/markup_snippets'),
    );
    
    // go back to the snippets list on submit
    $form['#redirect'] = 'admin/content/markup_snippets';
  
    return $form;
  }
  // otherwise send our bad request notice
  else {
    drupal_set_message(t('You must specify a valid snippet to delete.'), 'error');
    return $bad_request_form;
  }
}

/**
 * Submit handler for deleting snippets.
 */
function markup_snippets_admin_delete_submit($form, &$form_state) {
  db_query("DELETE FROM {markup_snippets} WHERE mid = %d", $form_state['values']['markup_snippets_mid']);
  drupal_set_message(t('Snippet deleted.'));
}

/**
 * Edit form for snippets.
 */
function markup_snippets_admin_edit() {
  
  // set up a default form if we get bad data
  $bad_request_form = array();
  $bad_request_form['#prefix'] = l(t('Go to snippet list'), 'admin/content/markup_snippets');
  
  // check we have a snippet ID
  if (is_numeric(arg(4))) {
    $mid = arg(4);
  }
  // if not send bad request
  else {
    drupal_set_message(t('You must specify a valid snippet to edit.'), 'error');
    return $bad_request_form;
  }
  
  // attempt to load the snippet
  $snippet = markup_snippets_get_snippets(array(), TRUE, $mid);
  
  // if there's a snippet, load the edit form
  if ($snippet) {
    $form = array();
    
    $form['markup_snippets_mid'] = array(
      '#type' => 'value',
      '#value' => $mid,
    );
    
    $form['markup_snippets_edit_name'] = array(
      '#type' => 'textfield',
      '#description' => t('The name of your snippet.'),
      '#title' => t('Name'),
      '#default_value' => $snippet[$mid]['name'],
    );
    
    $form['markup_snippets_edit_snippet'] = array(
      '#type' => 'textarea',
      '#description' => t('The actual snippet code.'),
      '#title' => t('Snippet'),
      '#default_value' => $snippet[$mid]['snippet'],
    );
  
    $form['markup_snippets_edit'] = array(
      '#type' => 'submit',
      '#default_value' => t('Save'),
    );
  
    $form['markup_snippets_edit_cancel'] = array(
      '#type' => 'markup',
      '#value' => l(t('Cancel'), 'admin/content/markup_snippets'),
    );
    
    // go back to the snippets list on submit
    $form['#redirect'] = 'admin/content/markup_snippets';
  
    return $form;
  }
  // otherwise send our bad request notice
  else {
    drupal_set_message(t('You must specify a valid snippet to edit.'), 'error');
    return $bad_request_form;
  }
}

/**
 * Validation handler for editing snippets.
 */
function markup_snippets_admin_edit_validate($form, &$form_state) {
  // make sure none of our fields are empty or filled with spaces
  if (trim($form_state['values']['markup_snippets_edit_name']) == '') {
    form_set_error('markup_snippets_edit_name', t('Name cannot be empty.'));
  }
  
  if (trim($form_state['values']['markup_snippets_edit_snippet']) == '') {
    form_set_error('markup_snippets_edit_snippet', t('Snippet cannot be empty.'));
  }
}

/**
 * Submit handler for saving edited snippets.
 */
function markup_snippets_admin_edit_submit($form, &$form_state) {
  // make sure the snippet name doesn't contain mark-up
  $snippet_name = check_plain($form_state['values']['markup_snippets_edit_name']);
  // save snippet
  db_query("UPDATE {markup_snippets} SET name = '%s', snippet = '%s' WHERE mid = %d", $snippet_name, $form_state['values']['markup_snippets_edit_snippet'], $form_state['values']['markup_snippets_mid']);
  drupal_set_message(t('Snippet saved.'));
}

/**
 * Admin page, presenting all the snippets in the database
 * in an HTML table.
 */
function markup_snippets_admin() {
  // load all snippets
  $snippets = markup_snippets_get_snippets();
  
  // if there are results
  if ($snippets) {
    // build our basic table data
    $header = array(t('Name'), '', '');
    $rows = array();
    
    // for each result build a table row
    foreach ($snippets as $mid => $snippet) {
      $rows[] = array(
        $snippet,
        l(t('Edit'), 'admin/content/markup_snippets/edit/'.$mid),
        l(t('Delete'), 'admin/content/markup_snippets/delete/'.$mid),
      );
    }
    
    // send back a themed HTML table
    return theme('table', $header, $rows);
  }
  // if there are no results, show this empty text
  else {
    $output = t('There are no snippets in the database.');
    return '<div>'.$output.'</div>';
  }
  
}

/**
 * Function for retrieving saved snippets from the database.
 * 
 * @param $snippets
 *  array - array of options to append data to, if required
 *  You might want to pass in an array with a default option set already
 * 
 * @param $values
 *  bool - whether to return the actual snippet value in the array
 * 
 * @param $mid
 *  int - the id for a specific snippet
 * 
 * @return
 *  array - snippets from the database, with or without actual
 *  snippet value included
 */
function markup_snippets_get_snippets($snippets = array(), $values = FALSE, $mid = NULL) {
  // check if we are selecting a single snippet
  if ($mid) {
    // if so, restrict query to that
    $query = db_query("SELECT * FROM {markup_snippets} WHERE mid = %d", $mid);
  }
  // otherwise get all snippets
  else {
    $query = db_query("SELECT * FROM {markup_snippets}");
  }
  
  // if there are no results send back a FALSE
  if (!$query) {
    return FALSE;
  }
  
  // otherwise loop through results
  while ($result = db_fetch_object($query)) {
    // check it we want the actual snippet mark-up
    if ($values) {
      $snippets[$result->mid] = array (
        'name' => t(check_plain($result->name)),
        'snippet' => $result->snippet,
      );
    }
    // otherwise just send back id and name (for select list options)
    else {
      $snippets[$result->mid] = t(check_plain($result->name));
    }
  }
  
  return $snippets;
}

/**
 * Implementation of ahah callback for node form.
 */
function markup_snippets_js() {
  // THIS CODE IS TAKEN FROM THIS HOWTO: http://drupal.org/node/331941
  // In Drupal 7 this code (up to our custom stuff) will be a core function
  
  // The form is generated in an include file which we need to include manually.
  // Changed from Poll module example:
  include_once drupal_get_path('module', 'node') . '/node.pages.inc';
  
  // AHAH has submitted our form, so we need to rebuild it
  // Note, this is changed from the example above - the example
  // causes an error because the node gets locked.
  
  // Preparing to retrieve form from cache
  // Old line:
  // $form_state = array('storage' => NULL, 'submitted' => FALSE);
  // Our replacement:
  $form_state = array('storage' => NULL, 'rebuild' => TRUE);
  $form_build_id = $_POST['form_build_id'];
  
  // Retrieve the form from the cache
  $form = form_get_cache($form_build_id, $form_state);

  // Prepare the form for processing
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  // Process the form
  drupal_process_form($form_id, $form, $form_state);
  // Rebuild the form
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  
  // CUSTOM CODE STARTS HERE
  
  // Now we have the latest $form, with the changes made by our
  // AHAH enabled field - we can now build our mark-up to return
  
  // get the node type info we need for our body field
  $node = $form['#node'];
  $type = node_get_types('type', $node);
  
  // load the snippet from the posted data
  $mid = $form_state['post']['markup_snippets_select'];
  $snippet = markup_snippets_get_snippets(array(), TRUE, $mid);
  
  // build the returned mark-up
  // we are mimicking the output of the node_get_body() function
  $output = '';
  $output .= '<label for="edit-body">' . check_plain($type->body_label) . ': </label>';
  $output .= '<div class="resizeable-textarea"><span>';
  $output .= '<textarea class="form-textarea resizable textarea-processed" id="edit-body" name="body" rows="20" cols="60">';
  // switch the body for our snippet
  $output .= $snippet[$mid]['snippet'];
  $output .= '</textarea>';
  $output .= '</span></div>';
  
  // ALL DONE

  // Final rendering callback - send our mark-up back to the div we
  // specified in our AHAH form information
  drupal_json(array('status' => TRUE, 'data' => $output));
  
}
